<?php

require_once('questions.php');

$questions = get_questions();

$score = 0;
$msg = "Bạn quá kém, cần ôn tập thêm.";
foreach ($questions as $q) {
    if ($q->get_true_answer() == $_COOKIE[$q->get_index()]) {
        ++$score;
    }
}
if ($score > 3 && $score < 8) {
    $msg = "Cũng bình thường";
}
else if ($score > 7) {
    $msg = "Sắp sửa làm được trợ giảng lớp PHP";
}
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Result</title>
    <link rel="stylesheet" href="styles.css"/>
    <link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>
    <script src= "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" ></script>
</head>
<body>
    <form>
        <?php 
        echo "<h4>Điểm của bạn: $score.</h4><br/><h4>Nhận xét: $msg</h4>";
        foreach($questions as $q) {
            echo $q->to_html_res($_COOKIE[$q->get_index()]);
        }
        ?>
    </form>
</body>
</html>