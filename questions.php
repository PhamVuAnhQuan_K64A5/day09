<?php
class Question {
    private $index;
    private $descr;
    private $answers;
    private $true_answer;

    function __construct($index, $descr, $answers, $true_answer) {
        $this->index = $index;
        $this->descr = $descr;
        $this->answers = $answers;
        $this->true_answer = $true_answer;
    }

    function get_name() {
        return "q$this->index";
    }

    function get_index() {
        return $this->index;
    }

    function get_true_answer() {
        return $this->true_answer;
    }

    function to_html() {
        $html_re = "<div class=\"question\">
            <div class=\"descr\">
                <p><b>Câu $this->index.</b> $this->descr</p>
            </div>
            <div class=\"answers\">";
        $c = 'a';
        $upper_c = strtoupper($c);
        foreach($this->answers as $answer) {
            $ans_div = "<div class=\"ans\">
                <input type=\"radio\" id=\"$this->index$c\" name=\"$this->index\" value=\"$c\" required>
                <label for=\"$this->index$c\">$upper_c. $answer</label>
            </div>";
            $html_re = $html_re . $ans_div;
            ++$c;
        }
        $html_re = $html_re . "</div></div>";
        return $html_re;
    }

    function to_html_res($ans_of_user) {
        $html_re = "<div class=\"question\">
            <div class=\"descr\">
                <p><b>Câu $this->index.</b> $this->descr</p>
            </div>
            <div class=\"answers\">";
        $c = 'a';
        $upper_c = strtoupper($c);
        foreach($this->answers as $answer) {
            $color = "black";
            $class_x = "";
            if ($c == $ans_of_user) {
                $color = "red";
                $class_x = "false_ans";
            }
            if ($c == $this->true_answer) {
                $color = "green";
                $class_x = "true_ans";
            }
            $ans_div = "<div class=\"ans\">
                <input type=\"radio\" id=\"$this->index$c\" name=\"$this->index\" value=\"$c\" required class=\"disabled-radio $class_x\">
                <label for=\"$this->index$c\" style=\"color:$color\">$upper_c. $answer</label></div>";
            $html_re = $html_re . $ans_div;
            ++$c;
        }
        $html_re = $html_re . "</div></div>";
        return $html_re;
    }

    // no getters and setters :)
}

function get_questions() {
    $q1 = new Question(1, "1 + 1 = ?", array(2, 3, 4, 5), "a");
    $q2 = new Question(2, "Thuật toán merge sort sử dụng kỹ thuật nào?", array("Quy hoạch động", "Quay lui", "Chia để trị", "Tham lam"), "c");
    $q3 = new Question(3, "Đội nào vô địch TI 11?", array("Team Liquid", "PSG.LGD", "Tundra", "Thunder Awaken"), "c");
    $q4 = new Question(4, "Đâu là tên con sư tử không thuộc liên minh Mapogo?", array("Kinky Tail", "Dark Name", "Mr. T", "Pretty Boy"), "b");
    $q5 = new Question(5, "Đâu không phải là một text editor?", array("Vim", "Micro", "Mental", "Emacs"), "c");
    $q6 = new Question(6, "2 + 2 = ?", array(2,3,4,5), "c");
    $q7 = new Question(7, "3 x 4 = ?", array(12,11,10,9), "a");
    $q8 = new Question(8, "4 + 1 = ?", array(2,3,4,5), "d");
    $q9 = new Question(9, "999 + 1 = ?", array(999,998,1000,1001), "c");
    $q10 = new Question(10, "1 x 0 = ?", array(1,0,3,4), "b");

    $questions = array($q1, $q2, $q3, $q4, $q5, $q6, $q7, $q8, $q9, $q10);
    return $questions;
}
?>