<?php

require_once('questions.php');

$questions = get_questions();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    foreach (array_keys($_POST) as $key) {
        setcookie($key, $_POST[$key], time() + 86400, "/");
    }
    header('location: second_page.php');
}
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page 1</title>
    <link rel="stylesheet" href="styles.css"/>
    <link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>
    <script src= "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" ></script>
</head>
<body>
    <form action="" method="POST">
        <?php
        $i = 0;
        while ($i < 5) {
            echo $questions[$i]->to_html();
            ++$i;
        }
        ?>
        <div class="submit-div">
            <input type="submit" value="Next"></input>
        </div>
    </form>   
</body>
</html>